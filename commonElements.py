def commonElements(list1, list2):

    for element in list1:
        if element not in list2:
            list1.remove(element)
    return list1


print(commonElements([1, 2, 3, 4, 5], [1, 3, 4]))


def common_elements(list1, list2):
    return list(set(list1).intersection(list2))


print(common_elements([1, 2, 3, 4, 5], [1, 3, 4]))

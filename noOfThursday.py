from datetime import date, timedelta


def thursdays(year1, year2):
    dt = date(1990, 1, 1)
    thursday = 0
    dt += timedelta(days=3 - dt.weekday())
    thursday += 1
    for year in range(year1, year2 + 1):
            while dt.year == year:
                # yield dt
                # print(dt.year)
                dt += timedelta(days=7)
                thursday += 1
    return thursday


print(thursdays(1990, 2000))

## Django setup

1. Create a `virtualenv` and run the following commands within the `virtualenv`
2. `pip install -r requirements.txt`
3. `python manage.py runserver`

## Data Analysis
Q.1 The factors that influence the marks the most

According to our  data analysis the factors that influence the marks the most are father education, father occupation, mother education, mother occupation and Help in household
There are also few other factors that influence the marks  these are Gender, Distance, Private Tuition, Watching TV and Siblings

Impact of:
Father Education: According to our data analysis if the father is Graduated or 
                  more educated  it influence the marks most but always this is not the case even if  the father is Illiterate still student can good in study father education also effect on individual subject marks.
            step: father education level can be of 6 type we calculated mean
                  values of marks group by father education level and the imrovement is calculated by substacting minimum mean value of each subject from maximum mean value

Father Occupation: Father occupation also effect the marks if the father is  
                   Professional, Teacher/Lecture student perform very well
            step: father education level can be of 9 type we calculated mean
                  values of marks group by father education level and the imrovement is calculated by substacting minimum mean value of each subject from maximum mean value
Mother Education: Our data analysis show that if the mother is Graduated or more
                  educated student is very good in reading and also good in Science, social amd math.
            step: mother education level can be of 6 type we calculated mean
                  values of marks group by father education level and the imrovement is calculated by substacting minimum mean value of each subject from maximum mean value

Mother Occupation: If the mother is Teacher/Lecturer by occupation student is  
                   good in Reading and also good in science, social and math and can improve the over all performance by +9.1%
            step: father education level can be of 9 type we calculated mean
                  values of marks group by father education level and the imrovement is calculated by substacting minimum mean value of each subject from maximum mean value
And also according to our analysis if the distance between school and house is less then it increase the perfornance

If student help in household everyday mostlikely it increase the performance

If watching TV regulerly it decrease the performance 

Q.2 How do boys and girls perform across states?

According to our data analysis Gender also has some impact on marks which may vary state wise. Boys and girls score different according to state

step: calculating mean of individual subjects marks for the state for both boys and girls  

Q.3 Do students from South Indian states really excel at Math and Science?
According to our data analysis students from North Indian states are good in Maths than South Indian students and the case is similer  for Science as well

step: if the students state is bolong to south Indian state compute the mean 
      for science and Maths marks and also comput the Science and Maths individual mean marks and the compare South Indian students marks with North Indians   

from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<state>[A-Z]+)$', views.boys_girls_perf_state, name='boys_girls_perf_state'),
    url(r'^south$', views.south_math_science, name='south_math_science'),
]
from django.apps import AppConfig


class GramenerConfig(AppConfig):
    name = 'gramener'

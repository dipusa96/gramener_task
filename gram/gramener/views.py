from django.shortcuts import render
from django.http import HttpResponse
from pandas import DataFrame
import matplotlib.pyplot as plt
import numpy as np


import pandas as pd
import mpld3
data1 = pd.read_csv('gramener/static/nas-pupil-marks.csv')
data = pd.read_csv('gramener/static/nas-labels.csv')


# print(data1)


# def index(request):
#     #dat = data.ix[[0, 211], ['Column', 'Rename']]
#     dat1 = data1.groupby(['Father edu']).std()
#     dat = data1.loc[data1.groupby(['Father edu'])].sum(axis=1)
#     # print(dat)
#     # st= np.std(dat)
#     print("Hi")
#     print(dat)
#     gh = plt.figure()
#     plt.plot(dat)
#     # mpld3.save_html(fig,"test.html")
#     html_fig = mpld3.fig_to_html(gh, template_type='simple')
#     plt.close()
#     return render(request, 'index.html', {'datal': html_fig})


def index(request):
    states = dict(zip(data['Level'], data['Rename']))
    dat1 = data1.groupby(['Father edu'])['Maths %','Reading %', 'Science %', 'Social %' ].mean()
    total = []
    math = dat1['Maths %'].tolist()
    math.sort()
    total.append(math[5] - math[0])
    print(total)
    reading = dat1['Reading %'].tolist()
    reading.sort()
    total.append(reading[5] - reading[0])
    science = dat1['Science %'].tolist()
    science.sort()
    total.append(science[5] - science[0])
    social = dat1['Social %'].tolist()
    social.sort()
    total.append(social[5] - social[0])
    total1 = ['%.2f' % round(elem, 1) for elem in total]
    total1 = [float(i) for i in total1]
    tot = round(np.mean(total), 1)
    y = [tot, total1[0], total1[1], total1[2], total1[3]]
    N = len(y)
    x = range(N)
    width = 0.3
    gh = plt.figure()
    barlist = plt.bar(x, y, width)
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    barlist[2].set_color('g')
    barlist[3].set_color('b')
    barlist[4].set_color('g')
    lables = ['Total', 'Math', 'Reading', 'Science', 'Social']
    plt.title('Father Education')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    html_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    dat1 = data1.groupby(['Father occupation'])['Maths %','Reading %', 'Science %', 'Social %' ].mean()
    total = []
    math = dat1['Maths %'].tolist()
    math.sort()
    total.append(math[8] - math[0])
    reading = dat1['Reading %'].tolist()
    reading.sort()
    total.append(reading[8] - reading[0])
    science = dat1['Science %'].tolist()
    science.sort()
    total.append(science[8] - science[0])
    social = dat1['Social %'].tolist()
    social.sort()
    total.append(social[8] - social[0])
    total1 = ['%.2f' % round(elem, 1) for elem in total]
    total1 = [float(i) for i in total1]
    tot = round(np.mean(total), 1)
    y = [tot, total1[0], total1[1], total1[2], total1[3]]
    N = len(y)
    x = range(N)
    width = 0.3
    gh = plt.figure()
    barlist = plt.bar(x, y, width)
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    barlist[2].set_color('g')
    barlist[3].set_color('b')
    barlist[4].set_color('g')
    lables = ['Total', 'Math', 'Reading', 'Science', 'Social']
    plt.title('Mother Education')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    fa_occu_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    dat1 = data1.groupby(['Mother edu'])['Maths %','Reading %', 'Science %', 'Social %' ].mean()
    total = []
    math = dat1['Maths %'].tolist()
    math.sort()
    total.append(math[5] - math[0])
    reading = dat1['Reading %'].tolist()
    reading.sort()
    total.append(reading[5] - reading[0])
    science = dat1['Science %'].tolist()
    science.sort()
    total.append(science[5] - science[0])
    social = dat1['Social %'].tolist()
    social.sort()
    total.append(social[5] - social[0])
    total1 = ['%.2f' % round(elem, 1) for elem in total]
    total1 = [float(i) for i in total1]
    tot = round(np.mean(total), 1)
    y = [tot, total1[0], total1[1], total1[2], total1[3]]
    N = len(y)
    x = range(N)
    width = 0.3
    gh = plt.figure()
    barlist = plt.bar(x, y, width)
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    barlist[2].set_color('g')
    barlist[3].set_color('b')
    barlist[4].set_color('g')
    lables = ['Total', 'Math', 'Reading', 'Science', 'Social']
    plt.title('Father Education')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    mother_edu_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    dat1 = data1.groupby(['Mother occupation'])['Maths %','Reading %', 'Science %', 'Social %' ].mean()
    total = []
    math = dat1['Maths %'].tolist()
    math.sort()
    total.append(math[8] - math[0])
    reading = dat1['Reading %'].tolist()
    reading.sort()
    total.append(reading[8] - reading[0])
    science = dat1['Science %'].tolist()
    science.sort()
    total.append(science[8] - science[0])
    social = dat1['Social %'].tolist()
    social.sort()
    total.append(social[8] - social[0])
    total1 = ['%.2f' % round(elem, 1) for elem in total]
    total1 = [float(i) for i in total1]
    tot = round(np.mean(total), 1)
    y = [tot, total1[0], total1[1], total1[2], total1[3]]
    N = len(y)
    x = range(N)
    width = 0.3
    gh = plt.figure()
    barlist = plt.bar(x, y, width)
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    barlist[2].set_color('g')
    barlist[3].set_color('b')
    barlist[4].set_color('g')
    lables = ['Total', 'Math', 'Reading', 'Science', 'Social']
    plt.title('Mother Occupation')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    mother_occu_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    return render(request, 'index.html', {'FatherEdu': html_fig,
                                          'FatherOccu': fa_occu_fig,
                                          'MotherEdu': mother_edu_fig,
                                          'MotherOccu': mother_occu_fig,
                                          'States': states})


def boys_girls_perf_state(request, state):
    
    boymarks = data1[(data1['State'] == state) & (data1['Gender'] == 1)].sum()['Maths %']
    boy_count = data1[(data1['State'] == state) & (data1['Gender'] == 1)].count()['Maths %']
    girlmarks = data1[(data1['State'] == state) & (data1['Gender'] == 2)].sum()['Maths %']
    girl_count = data1[(data1['State'] == state) & (data1['Gender'] == 2)].count()['Maths %']
    y = [boymarks / boy_count, girlmarks / girl_count]
    N = len(y)
    x = range(N)
    width = 1 / 1.5
    gh = plt.figure()
    barlist = plt.bar(x, y, width, align='center')
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    lables = ['Boys', 'Girls']
    plt.title('Math %')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    html_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    boymarks = data1[(data1['State'] == state) & (data1['Gender'] == 1)].sum()['Reading %']
    boy_count = data1[(data1['State'] == state) & (data1['Gender'] == 1)].count()['Reading %']
    girlmarks = data1[(data1['State'] == state) & (data1['Gender'] == 2)].sum()['Reading %']
    girl_count = data1[(data1['State'] == state) & (data1['Gender'] == 2)].count()['Reading %']
    y = [boymarks / boy_count, girlmarks / girl_count]
    N = len(y)
    x = range(N)
    width = 1 / 1.5
    gh = plt.figure()
    barlist = plt.bar(x, y, width, align='center')
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    lables = ['Boys', 'Girls']
    plt.title('Readings %')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    reading_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    boymarks = data1[(data1['State'] == state) & (data1['Gender'] == 1)].sum()['Science %']
    boy_count = data1[(data1['State'] == state) & (data1['Gender'] == 1)].count()['Science %']
    girlmarks = data1[(data1['State'] == state) & (data1['Gender'] == 2)].sum()['Science %']
    girl_count = data1[(data1['State'] == state) & (data1['Gender'] == 2)].count()['Science %']
    y = [boymarks / boy_count, girlmarks / girl_count]
    N = len(y)
    x = range(N)
    width = 1 / 1.5
    gh = plt.figure()
    barlist = plt.bar(x, y, width, align='center')
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    lables = ['Boys', 'Girls']
    plt.title('Science %')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    science_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    boymarks = data1[(data1['State'] == state) & (data1['Gender'] == 1)].sum()['Social %']
    boy_count = data1[(data1['State'] == state) & (data1['Gender'] == 1)].count()['Social %']
    girlmarks = data1[(data1['State'] == state) & (data1['Gender'] == 2)].sum()['Social %']
    girl_count = data1[(data1['State'] == state) & (data1['Gender'] == 2)].count()['Social %']
    y = [boymarks / boy_count, girlmarks / girl_count]
    N = len(y)
    x = range(N)
    width = 1 / 1.5
    gh = plt.figure()
    barlist = plt.bar(x, y, width, align='center')
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    lables = ['Boys', 'Girls']
    plt.title('Social %')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    social_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()

    return render(request, 'boygirl.html', {'maths': html_fig,
                                            'State': state,
                                            'Reading': reading_fig,
                                            'Science': science_fig,
                                            'Social': social_fig})


def south_math_science(request):
    ST = ['AN', 'AP', 'KA', 'KL', 'TN', 'PY', 'AN']
    NT = ['AR', 'BR', 'CG', 'CH', 'DD', 'DL', 'DN',
          'GA', 'GJ', 'HP', 'HR', 'JH', 'JK', 'MG',
          'MH', 'MN', 'MZ', 'NG', 'OR', 'PB', 'RJ',
          'SK', 'TR', 'UK', 'UP', 'WB']

    st_math_marks = data1[data1['State'].isin(ST)].sum()['Maths %']
    st_stu_count = data1[data1['State'].isin(ST)].count()['Maths %']
    nt_math_marks = data1[data1['State'].isin(NT)].sum()['Maths %']
    nt_stu_count = data1[data1['State'].isin(NT)].count()['Maths %']
    y = [st_math_marks / st_stu_count, nt_math_marks / nt_stu_count]
    N = len(y)
    x = range(N)
    width = 1 / 1.5
    gh = plt.figure()
    barlist = plt.bar(x, y, width, align='center')
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    lables = ['South', 'North']
    plt.title('Math %')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    math_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    st_science_marks = data1[data1['State'].isin(ST)].sum()['Science %']
    st_stu1_count = data1[data1['State'].isin(ST)].count()['Science %']
    nt_science_marks = data1[data1['State'].isin(NT)].sum()['Science %']
    nt_stu1_count = data1[data1['State'].isin(NT)].count()['Science %']
    y = [st_science_marks / st_stu1_count, nt_science_marks / nt_stu1_count]
    N = len(y)
    x = range(N)
    width = 1 / 1.5
    gh = plt.figure()
    barlist = plt.bar(x, y, width, align='center')
    barlist[0].set_color('r')
    barlist[1].set_color('b')
    lables = ['South', 'North']
    plt.title('Science %')
    plt.xticks(x, lables)
    for a, b in zip(x, y):
        plt.text(a, b, str(b), fontweight='bold')
    science_fig = mpld3.fig_to_html(gh, template_type='simple')
    plt.close()
    return render(request, 'southMathScience.html', {'Math': math_fig,
                                                     'Science': science_fig})

